GeoHack
=======

Source code for https://tools.wmflabs.org/geohack/geohack.php

Documentation: https://www.mediawiki.org/wiki/GeoHack
